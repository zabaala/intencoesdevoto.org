<?php

use Faker\Generator as Faker;

$factory->define(App\Domains\Affiliations\Affiliation::class, function (Faker $faker) {
    return [
        'name' => $faker->name()
    ];
});