## Sobre o projeto

**Intenções de Voto (intencoesdevoto.org)** é um projeto livre, de código aberto e apartidário e sem fins lucrativos, 
desenvolvido para capturar as intenções de votos da população Brasileira para as eleições presidenciais de 2018, 
não possuindo nenhuma relação com centros de pesquisas, orgãos e organizações governamentais e não-governamentais, 
partidos políticos, tribunais ou entidades.

A infraestrutura necessária para o funcionamento deste projeto é mantida com recursos próprios de seu criador, que não 
aceita nenhum tipo de donativo para este ou qualquer outro fim.

## Funcionamento

O site Inteções de Voto (intencoesdevoto.org) apresenta, para todos os visitantes do site, os candidatos anunciados no 
ano da eleição. Todos os candidatos possuem um perfil, onde são apresentadas as intenções de votos, classificadas por 
região.

O participante que desejar informar sua intenção, deve se cadastrar no site intencoesdevoto.org, utilizando uma conta 
social de sua propriedade (Facebook, Google, Instagram ou Twitter),e seguir os seguintes passos:

1. Selecionar o candidato;
2. Informar estado e município de votação;
3. Confirmar intenção de voto.

## Sistema antifraude

Por se tratar de questões políticas, algumas medidas serão tomadas para que scripts ou comportamentos
maliciosos possam comprometer a integridade dos dados e dos participantes. 

Até o momento, as medidas adotadas são as seguintes:

- Só serão aceitos perfis sociais criados há, pelo menos, 1 (um) ano da data da informação da intenção de voto;
- Para confirmar um voto, é necessária confirmar que o participante não é um robô, através de um captcha.

## Domínios válidos do projeto

Todos os domínios abaixo listados, são domínios válidos do projeto Intenções de Voto. 

São eles:

- intencoesdevoto.org (principal)
- intencoesdevoto.net
- intencoesdevoto.info
- intencoesdevoto.com
- intencoesdevoto.com.br

## Contribuição

Queremos tonar esse projeto melhor e, se você puder, ficaremos felizes com a sua contribuição. Envie-nos um pull 
request, por favor. Ele será visto e avaliado com muito carinho e atenção.

## Vulnerabilidades

Se você encontrou alguma vulnerabilidade neste projeto, por favor, envie um e-mail para contato@intencoesdevoto.org. 
Teremos prazer em recebê-las, resolvê-las e menciar você como colaborador.  

## Licença

Este projeto está sobre a licença [MIT license](https://opensource.org/licenses/MIT).
