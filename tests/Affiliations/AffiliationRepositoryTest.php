<?php

namespace Tests\Affiliations;

use App\Domains\Affiliations\Affiliation;
use App\Domains\Affiliations\AffiliationRepository;
use Faker\Factory;
use Faker\Generator;
use Tests\TestCase;

class AffiliationRepositoryTest extends TestCase
{
    const TABLE_NAME = 'affiliations';

    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    public function test_create_affiliation_by_name()
    {
        $name = $this->faker->name();
        $repository = (new AffiliationRepository)->createNewAffiliationWithName($name);

        $this->assertTrue(is_array($repository));

        $this->assertEquals($name, $repository['name']);
    }

    public function test_update_a_affiliation_by_id()
    {
        $affiliation = \factory(Affiliation::class)->create();
        $newName = $this->faker->name();

        $repository = (new AffiliationRepository)->updateById($affiliation->id, ['name' => $newName]);

        $this->assertTrue(is_array($repository));

        $this->assertEquals($newName, $repository['name']);
        $this->assertEquals($affiliation->id, $repository['id']);
    }


    /**
     * @expectedException \TypeError
     */
    public function test_id_argument_passed_to_updateById_method()
    {
        (new AffiliationRepository)->updateById('foo', []);
    }

    /**
     * @expectedException \TypeError
     */
    public function test_data_argument_passed_to_updateById_method()
    {
        (new AffiliationRepository)->updateById(1, 'foo');
    }

    public function test_get_a_affiliation_by_id()
    {
        $affiliations = \factory(Affiliation::class, 20)->create();
        $repository = (new AffiliationRepository())->findById(18);

        $affiliation = $affiliations->firstWhere('id', 18);

        $this->assertTrue(is_array($repository));

        $this->assertEquals($affiliation->id, $repository['id']);
        $this->assertEquals($affiliation->name, $repository['name']);
        $this->assertEquals($affiliation->created_at, $repository['created_at']);
        $this->assertEquals($affiliation->updated_at, $repository['updated_at']);
    }

    /**
     * @expectedException \TypeError
     */
    public function test_delete_a_affiliation_passing_a_invalid_argument_id()
    {
        (new AffiliationRepository)->deleteById('foo');
    }

    public function test_delete_a_affiliation_by_id()
    {
        $affiliation = \factory(Affiliation::class)->create();

        $repository = (new AffiliationRepository)->deleteById($affiliation->id);

        $this->assertTrue($repository);
    }

    /**
     * @expectedException \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function test_delete_a_affiliation_by_id_passing_a_nonexistent_affiliation_id()
    {
        (new AffiliationRepository)->deleteById(10);
    }
}
