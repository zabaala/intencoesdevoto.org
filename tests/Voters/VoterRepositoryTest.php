<?php

namespace Tests\Voters;

use App\Domains\Voter\Interfaces\VoterAttributesInterface;
use App\Domains\Voter\Voter;
use App\Domains\Voter\VoterRepository;
use Faker\Factory;
use Faker\Generator;
use Tests\TestCase;

class VoterRepositoryTest extends TestCase implements VoterAttributesInterface
{
    /**
     * @var Generator
     */
    private $faker;

    public function setUp()
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage Undefined index: avatar
     */
    public function test_create_a_new_voter_without_avatar()
    {
        $repository = new VoterRepository();
        $repository->createNewVoter([]);
    }

    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage Undefined index: email
     */
    public function test_create_a_new_voter_without_email()
    {
        $repository = new VoterRepository();
        $repository->createNewVoter([self::VOTER_ATTR_AVATAR => $this->faker->imageUrl()]);
    }

    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage Undefined index: social_network
     */
    public function test_create_a_new_voter_without_social_network()
    {
        $repository = new VoterRepository();
        $repository->createNewVoter([
           self::VOTER_ATTR_EMAIL => $this->faker->email,
            self::VOTER_ATTR_AVATAR => $this->faker->imageUrl()
        ]);
    }

    /**
     * @expectedException \ErrorException
     * @expectedExceptionMessage Undefined index: social_network_id
     */
    public function test_create_a_new_voter_without_social_network_id()
    {
        $repository = new VoterRepository();
        $repository->createNewVoter([
            self::VOTER_ATTR_EMAIL => $this->faker->email,
            self::VOTER_ATTR_AVATAR => $this->faker->imageUrl(),
            self::VOTER_ATTR_SOCIAL_NETWORK => Voter::SOCIAL_NETWORK_FACEBOOK
        ]);
    }

    public function test_passed_data_to_create_new_voter_method()
    {
        $data = [
            self::VOTER_ATTR_EMAIL => $this->faker->email,
            self::VOTER_ATTR_AVATAR => $this->faker->imageUrl(),
            self::VOTER_ATTR_SOCIAL_NETWORK => Voter::SOCIAL_NETWORK_FACEBOOK,
            self::VOTER_ATTR_SOCIAL_NETWORK_ID => $this->faker->numberBetween(1),
        ];

        $voter = (new VoterRepository)->createNewVoter($data);

        $this->assertEquals($data[self::VOTER_ATTR_EMAIL], $voter->email);
        $this->assertEquals($data[self::VOTER_ATTR_SOCIAL_NETWORK], $voter->social_network);
        $this->assertEquals($data[self::VOTER_ATTR_SOCIAL_NETWORK_ID], $voter->social_network_id);
        $this->assertEquals($data[self::VOTER_ATTR_AVATAR], $voter->avatar);
    }
}
