<?php

namespace App\Domains\Voter\Interfaces;

interface VoterAttributesInterface
{
    const VOTER_ATTR_AVATAR = 'avatar';
    const VOTER_ATTR_EMAIL = 'email';
    const VOTER_ATTR_SOCIAL_NETWORK = 'social_network';
    const VOTER_ATTR_SOCIAL_NETWORK_ID = 'social_network_id';

    const SOCIAL_NETWORK_FACEBOOK = 'facebook';
    const SOCIAL_NETWORK_GOOGLE = 'google';
    const SOCIAL_NETWORK_TWITTER = 'twitter';
}
