<?php

namespace App\Domains\Voter\Migrations;

use App\Support\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVotersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('voters', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('public_id')->unique();
            $table->string('email', 60)->unique();
            $table->string('avatar')->nullable();
            $table->bigInteger('social_network_id');
            $table->enum('social_network', ['facebook', 'google', 'twitter']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('voters');
    }
}
