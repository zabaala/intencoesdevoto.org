<?php

namespace App\Domains\Voter;

use Ramsey\Uuid\Uuid;

class VoterRepository
{
    /**
     * Create new voter from array.
     *
     * @param array $data
     * @return Voter
     */
    public function createNewVoter(array $data)
    {
        $voter = new Voter();
        $voter->public_id = Uuid::uuid1()->toString();
        $voter->avatar = $data['avatar'];
        $voter->email = $data['email'];
        $voter->social_network = $data['social_network'];
        $voter->social_network_id = $data['social_network_id'];
        $voter->save();

        return $voter;
    }
}
