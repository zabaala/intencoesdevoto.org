<?php

namespace App\Domains\Voter;

use App\Domains\Voter\Interfaces\VoterAttributesInterface;
use Illuminate\Database\Eloquent\Model;

class Voter extends Model implements VoterAttributesInterface
{
    /**
     * @var string
     */
    protected $table = 'voters';

    /**
     * @var array
     */
    protected $fillable = [
        'avatar',
        'email',
        'social_network',
        'social_network_id',
    ];
}
