<?php

namespace App\Domains\Voter\Providers;

use App\Domains\Voter\Migrations\CreateVotersTable;
use App\Support\Migration\MigrationServiceProvider;

class VoterServiceProvider extends MigrationServiceProvider
{
    public function register()
    {
        $this->migrations([
            CreateVotersTable::class
        ]);
    }
}
