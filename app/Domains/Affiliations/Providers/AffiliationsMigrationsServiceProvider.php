<?php

namespace App\Domains\Affiliations\Providers;

use App\Domains\Affiliations\Migrations\CreateAffiliationsTable;
use App\Support\Migration\MigrationServiceProvider;

class AffiliationsMigrationsServiceProvider extends MigrationServiceProvider
{
    public function register()
    {
        $this->migrations([
            CreateAffiliationsTable::class
        ]);
    }

}
