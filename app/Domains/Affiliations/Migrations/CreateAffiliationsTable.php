<?php

namespace App\Domains\Affiliations\Migrations;

use App\Support\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAffiliationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('affiliations', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->dropIfExists('affiliations');
    }
}
