<?php

namespace App\Domains\Affiliations;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class AffiliationRepository
{
    /**
     * @param int $id
     * @return array
     */
    public function findById(int $id)
    {
        return Affiliation::find($id)->toArray();
    }

    /**
     * @param $name
     * @return array
     */
    public function createNewAffiliationWithName($name)
    {
        $affiliation = new Affiliation();
        $affiliation->name = $name;
        $affiliation->save();

        return $affiliation->toArray();
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function updateById(int $id, array $data)
    {
        $affiliation = Affiliation::find($id);
        $affiliation->name = $data['name'];
        $affiliation->save();

        return $affiliation->toArray();
    }

    /**
     * @param int $id
     * @return mixed
     *
     * @throws ModelNotFoundException
     */
    public function deleteById(int $id)
    {
        return Affiliation::findOrFail($id)->delete();
    }
}
