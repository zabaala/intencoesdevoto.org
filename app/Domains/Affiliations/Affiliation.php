<?php

namespace App\Domains\Affiliations;

use Illuminate\Database\Eloquent\Model;

class Affiliation extends Model
{
    /**
     * @var string
     */
    protected $table = 'affiliations';
}
