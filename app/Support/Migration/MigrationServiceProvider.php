<?php

namespace App\Support\Migration;

use Illuminate\Support\ServiceProvider;
use Migrator\MigratorTrait;

class MigrationServiceProvider extends ServiceProvider
{
    use MigratorTrait;
}
